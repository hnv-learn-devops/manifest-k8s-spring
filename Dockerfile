FROM maven:3-jdk-8-alpine

WORKDIR /usr/src/app

COPY . /usr/src/app
RUN mvn package

ENV MAIN_RUN=dev
CMD [ "sh", "-c", "mvn -Dspring-boot.run.profiles=${MAIN_RUN} spring-boot:run" ]
